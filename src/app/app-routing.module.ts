import {ModuleWithProviders, NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LayoutComponent} from "./shared/layout/layout.component";
import {AuthComponent} from "./auth/auth.component";
import {AuthGuardService as AuthGuard} from "./services/auth-guard.service";
import {LoginConfirmationComponent} from "./auth/login-confirmation/login-confirmation.component";

const routes: Routes = [
  {
    path:'',
    component: LayoutComponent,
    data:{ pageTitle:'İnternet Şubesi' },
    children:[
      {
        path:'',
        component:AuthComponent,
        data:{ pageTitle:'Giriş' }
      },
      {
        path:'giris-dogrulama',
        component:LoginConfirmationComponent,
        data:{ pageTitle:'Doğrulama' }
      }
    ]
  },
  {
    path:'kurumsal',
    canActivate:[AuthGuard],
    canActivateChild:[AuthGuard],
    component: LayoutComponent,
    data:{ pageTitle: 'İnternet Şubesi' },
    children:[
      {
        path:'',
        loadChildren:()=>import('./dashboard/dashboard.module').then(m=>m.DashboardModule),
        data: { pageTitle: 'Hoşgeldiniz' }
      }
    ]
  }
];

export const approuting:ModuleWithProviders<any> = RouterModule.forRoot(routes,{useHash:false, anchorScrolling:'enabled'});
