import {Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import * as globals from "../globals";
import {Title} from "@angular/platform-browser";
import {VirtualKeyboardDirective} from "../shared/layout/ui/virtual-keyboard/virtual-keyboard.directive";
import {NotificationService} from "../services/notification.service";
declare var $:any;
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  public input:any;
  public username:any;
  public password:any;

  constructor(public titleService:Title, public route:ActivatedRoute, public notification:NotificationService, public router:Router) {
    let title = globals.siteTitle+(route.snapshot.data.pageTitle?' | '+route.snapshot.data.pageTitle:'');
    titleService.setTitle(title);
  }

  ngOnInit(): void {
  }

  login(){
    if(this.username=="Kerim" && this.password=="test") {
      localStorage.setItem("username", this.username);
      localStorage.setItem("password", this.password);
      this.router.navigate(['giris-dogrulama']);
    }else{
      this.notification.alertBox({
        icon : 'warning',
        title : 'Giriş Bilgileri Hatalı!',
        html : 'Tekrar deneyiniz.'
      });
    }
  }

  toggleVirtualKeyboard(el:any){
    if(this.input){
      el.toggle();
    }
  }
}
