import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-confirmation',
  templateUrl: './login-confirmation.component.html',
  styleUrls: ['./login-confirmation.component.scss']
})
export class LoginConfirmationComponent implements OnInit, OnDestroy {

  public timer:any;
  public now:any;
  public min:any;
  public sec:any;
  public t:any;
  public deg:any;
  public per:any;
  public counttime:any=30000;

  constructor(public router:Router) { }

  ngOnInit(): void {

    let datetime = new Date().getTime()+this.counttime;

    this.timer = setInterval(()=>{
      this.now = new Date().getTime();
      let milisec_diff = datetime - this.now;
      let datediff = new Date(milisec_diff);
      if(milisec_diff>0) {
        this.min = this.twoDigit(datediff.getUTCMinutes());
        this.sec = this.twoDigit(datediff.getUTCSeconds());
        this.per = (milisec_diff/this.counttime)*100;
        this.deg = (milisec_diff/this.counttime)*360;
      }
      if(milisec_diff==0){
        this.per=100;
        this.deg=180;
        clearInterval(this.timer);
        this.userLoggedIn();
      }
    },10);
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }

  userLoggedIn(){
    localStorage.setItem('currentusertoken', 'testuser');
    this.router.navigate(['/kurumsal']);
  }

  private twoDigit(number: number) {
    return number > 9 ? "" + number: "0" + number;
  }

}
