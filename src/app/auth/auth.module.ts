import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import {UiModule} from "../shared/layout/ui/ui.module";
import {FormsModule} from "@angular/forms";
import { LoginConfirmationComponent } from './login-confirmation/login-confirmation.component';



@NgModule({
  declarations: [AuthComponent, LoginConfirmationComponent],
  imports: [
    CommonModule,
    UiModule,
    FormsModule
  ]
})
export class AuthModule { }
