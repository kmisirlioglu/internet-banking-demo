import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import {dashboardrouting} from "./dashboard-routing.module";
import {DragDropModule} from "@angular/cdk/drag-drop";
import {ChartsModule} from "ng2-charts";



@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    dashboardrouting,
    DragDropModule,
    ChartsModule
  ]
})
export class DashboardModule { }
