import {AfterViewInit, ChangeDetectorRef, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {ChartType} from "chart.js";
import {Label, MultiDataSet} from "ng2-charts";

declare var $:any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  public dragItems:Array<any>;
  @ViewChild('item1') item1:TemplateRef<any>;
  @ViewChild('item2') item2:TemplateRef<any>;
  @ViewChild('item3') item3:TemplateRef<any>;
  @ViewChild('item4') item4:TemplateRef<any>;
  @ViewChild('item5') item5:TemplateRef<any>;
  @ViewChild('item6') item6:TemplateRef<any>;

  public chartLabels:Label[] = ['Vadesiz TL', 'Vadesiz USD', 'Vadeli TL']

  public chartData: MultiDataSet = [
    [15000, 15000, 30000]
  ];

  public chartType: ChartType = 'doughnut';

  constructor(private ref: ChangeDetectorRef){}

  ngOnInit(): void {
    this.dragItems = new Array<any>('item1','item2','item3','item4','item5','item6');
    let saveddata = localStorage.getItem('items')?JSON.parse(localStorage.getItem('items')):null;
    if(saveddata) this.dragItems = saveddata;
  }

  ngAfterViewInit() {
    this.ref.detectChanges();
    setTimeout(()=>{
      $('.carousel').each((i:any,e:any)=>{
        $(e).carousel();
        $('.carousel-control-next',e).click(()=>{$(e).carousel('next');});
        $('.carousel-control-prev',e).click(()=>{$(e).carousel('prev');});
      });
    },250)

  }

  drop(event:CdkDragDrop<any>){
    //moveItemInArray(this.dragItems, event.previousIndex, event.currentIndex);
    /*console.log(event.previousContainer.data.index, event.container.data.index);*/
    this.dragItems[event.previousContainer.data.index]=event.container.data.item;
    this.dragItems[event.container.data.index]=event.previousContainer.data.item;
    localStorage.setItem('items',JSON.stringify(this.dragItems))
  }

}
