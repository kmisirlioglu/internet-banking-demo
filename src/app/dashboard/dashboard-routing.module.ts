import {ModuleWithProviders, NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./dashboard.component";

const routes: Routes = [
  {
    path:'',
    component: DashboardComponent,
    data:{ pageTitle:'Hoşgeldiniz' },
  }
];

export const dashboardrouting:ModuleWithProviders<any> = RouterModule.forChild(routes);
