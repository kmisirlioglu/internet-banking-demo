import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import {RouterModule} from "@angular/router";
import {HeaderModule} from "./header/header.module";
import {FooterModule} from "./footer/footer.module";
import {NavigationModule} from "./navigation/navigation.module";
import {NotificationService} from "../utils/notification.service";
import {UiModule} from "./ui/ui.module";



@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    HeaderModule,
    FooterModule,
    RouterModule,
    NavigationModule,
    UiModule
  ],
  providers:[
    NotificationService
  ]
})
export class LayoutModule { }
