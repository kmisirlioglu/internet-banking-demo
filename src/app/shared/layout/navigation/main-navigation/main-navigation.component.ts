import { Component, OnInit } from '@angular/core';
import {ComponentCommunication} from "../../../../services/component-communication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent implements OnInit {
  public isLoggedIn:any=false;
  constructor(public service:ComponentCommunication, public router:Router) { }

  ngOnInit(): void {
    this.service.authenticated.subscribe((v:any)=>{
      this.isLoggedIn = v;
    });
    this.service.checkAuthenticated();
    this.isLoggedIn = this.service.authenticatedVal;
  }

}
