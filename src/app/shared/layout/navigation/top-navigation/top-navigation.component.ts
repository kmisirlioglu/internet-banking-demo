import { Component, OnInit } from '@angular/core';
import {ComponentCommunication} from "../../../../services/component-communication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-top-navigation',
  templateUrl: './top-navigation.component.html',
  styleUrls: ['./top-navigation.component.scss']
})
export class TopNavigationComponent implements OnInit {
  public isLoggedIn:any=false;
  constructor(public service:ComponentCommunication, public router:Router) { }

  ngOnInit(): void {
    this.service.authenticated.subscribe((v:any)=>{
      this.isLoggedIn = v;
    });
    this.service.checkAuthenticated();
    this.isLoggedIn = this.service.authenticatedVal;
  }

  logout(){
    localStorage.removeItem('currentusertoken');
    localStorage.removeItem('username');
    localStorage.removeItem('password');
    this.service.checkAuthenticated();
    this.router.navigate(["/"]);
  }

}
