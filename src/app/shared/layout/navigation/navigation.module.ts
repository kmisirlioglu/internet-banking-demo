import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MainNavigationComponent} from "./main-navigation/main-navigation.component";
import {TopNavigationComponent} from "./top-navigation/top-navigation.component";



@NgModule({
  declarations: [ MainNavigationComponent, TopNavigationComponent],
  imports: [
    CommonModule
  ],
  exports:[MainNavigationComponent,TopNavigationComponent]
})
export class NavigationModule { }
