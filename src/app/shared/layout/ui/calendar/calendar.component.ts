import {AfterViewInit, Component, Input, OnInit} from '@angular/core';

declare var FullCalendar:any;
declare var $:any;
declare var toastr:any;

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, AfterViewInit {

  @Input() events:Array<any> = new Array<any>();

  public date = new Date();

  public options:any = {
    initialView: 'dayGridMonth',
    headerToolbar : {
      left:'prev,next today',
      center:'title',
      right : 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    events:[],
    /*eventContent: (event:any) => {
      console.log(event);
      if(event.event.extendedProps && event.event.extendedProps.icon) {
        //$(event.el).find(".fc-title").prepend("<i class='"+event.event.extendedProps.icon+"'></i> ");
      }
      let arrayOfDomNodes = [];
      return { domNodes: arrayOfDomNodes };
    },*/
    eventClick: function(calEvent:any) {
      toastr.info(
        calEvent.event.extendedProps.description,
        calEvent.event.title
      );
    },
    editable:false,
    droppable:false,
    locale:'tr'
  };

  public calenderEl:any;
  public calendar:any;

  constructor() { }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.calenderEl = $('#dashboard-calendar').get(0);
    this.options.events = this.events;
    this.calendar = new FullCalendar.Calendar(this.calenderEl, this.options);
    this.calendar.render();
  }

}
