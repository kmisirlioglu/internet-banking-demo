
import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit, Optional
} from "@angular/core";
import {FormControlDirective, NgControl, NgModel} from "@angular/forms";
import Keyboard from 'simple-keyboard';

declare var $:any;

@Directive({
  selector:'[virtualKeyboard]',
  providers: [NgModel,FormControlDirective]
})
export class VirtualKeyboardDirective implements OnInit,OnDestroy,AfterViewInit,OnChanges{
  @Input() virtualKeybord:any;
  @Input() input:any;
  public value:any;
  public keyboard:any;

  constructor(private el:ElementRef) {}

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  ngAfterViewInit() {
    this.el.nativeElement.toggle=()=>this.toggle();
  }

  ngOnChanges() {
    if(this.keyboard) this.keyboard.setInput(this.input.value);
  }

  toggle(){
    if(!this.keyboard) {
      $(this.input.valueAccessor._elementRef.nativeElement).on("change",this.onInputChange);
      $(this.input.valueAccessor._elementRef.nativeElement).on("keypress",this.onKeyPress);
      this.keyboard = new Keyboard({
        onChange: input => this.onChange(input),
        onKeyPress: button => this.onKeyPress(button)
      });
      this.keyboard.setInput(this.input.value);
    }else{
      $(this.input.valueAccessor._elementRef.nativeElement).off("change",this.onInputChange);
      $(this.input.valueAccessor._elementRef.nativeElement).off("keypress",this.onKeyPress);
      this.keyboard.destroy();
      this.keyboard=null;
    }
  }

  onChange(input: string) {
    this.input.control.setValue(input);
  };

  onKeyPress(button: string){
    if (button === "{shift}" || button === "{lock}") this.handleShift();
  };

  onInputChange(event: any){
    this.keyboard.setInput(event.target.value);
  };

  handleShift(){
    let currentLayout = this.keyboard.options.layoutName;
    let shiftToggle = currentLayout === "default" ? "shift" : "default";

    this.keyboard.setOptions({
      layoutName: shiftToggle
    });
  };
}
