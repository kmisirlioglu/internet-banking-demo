import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar/calendar.component';
import {VirtualKeyboardDirective} from "./virtual-keyboard/virtual-keyboard.directive";



@NgModule({
  declarations: [CalendarComponent, VirtualKeyboardDirective],
  exports:[CalendarComponent, VirtualKeyboardDirective],
  imports: [
    CommonModule
  ]
})
export class UiModule { }
