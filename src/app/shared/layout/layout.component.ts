import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ComponentCommunication} from "../../services/component-communication.service";
import {Router} from "@angular/router";

declare var $:any;

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {
  public isLoggedIn=false;
  private start:any;
  private vw:any = 0;
  private vh:any = 0;

  public events:Array<any> = new Array<any>();

  constructor(public service:ComponentCommunication, public router:Router) { }

  ngOnInit(): void {
    this.service.authenticated.subscribe((v:any)=>{
      this.isLoggedIn = v;
      if(this.isLoggedIn) this.router.navigate(['/kurumsal']);
    });
    this.service.checkAuthenticated();
    this.isLoggedIn = this.service.authenticatedVal;
    this.events.push({
      title          : 'Kredi Ödemesi',
      description    : '₺2605 tutarında 5. taksit ödemesi bulunmaktadır.',
      start          : '2021-01-29',
      allDay         : true,
      backgroundColor: '#f56954',
      borderColor    : '#f56954',
      icon           : 'fas fa'
    });
  }

  ngAfterViewInit() {
    window.requestAnimationFrame(this.viewPortCalc.bind(this));
  }

  viewPortCalc(timestamp:any){
    if (this.start === undefined) {
      this.start = timestamp;
    }
    let elapsed:any = timestamp - this.start;
    if (elapsed >= 250) {
      let vw = window.innerWidth * 0.01
      let vh = window.innerHeight * 0.01;
      if(vw != this.vw || vh != this.vh) {
        document.documentElement.style.setProperty('--vw', vw + "px");
        document.documentElement.style.setProperty('--vh', vh + "px");
        this.vw = vw;
        this.vh = vh;
      }
      this.start=undefined;
    }
    window.requestAnimationFrame(this.viewPortCalc.bind(this));
  }
}
