import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SafeHtmlPipe} from "./safe-html.pipe";
import {NotificationService} from "./notification.service";

@NgModule({
  declarations: [SafeHtmlPipe],
  exports:[SafeHtmlPipe],
  imports: [
    CommonModule
  ],
  providers:[NotificationService]
})
export class UtilsModule {
  static forRoot(){
    return {
      ngModule: UtilsModule,
      providers: [NotificationService]
    }
  }
}
