import {Injectable} from '@angular/core';

declare var $:any;
declare var toastr:any;
declare var Swal:any;

@Injectable()
export class NotificationService {

  private toastrdefaults:any={
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  constructor() {
  }

  notify(data:any, cb?:any) {
    var opt = $.extend({}, this.toastrdefaults, data);
    if(cb) opt.onHidden = cb;
    toastr.options = opt;
    switch(data.type){
      case 'success':
        toastr.success(data.message);
        break;
      case 'info':
        toastr.info(data.message);
        break
      case 'warning':
        toastr.warning(data.message);
        break;
      case 'error':
        toastr.error(data.message);
        break;
    }
  }

  alertBox(data:any, cb?:any) {
    Swal.fire(data).then((result:any)=>{ if(cb) cb(result); })
  }

}
