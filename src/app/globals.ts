import { environment } from '../environments/environment';

export const env = environment;
declare var $: any;
export const siteTitle:string = "İnternet Şubesi";

export const ErrorActions = {
  401:{ message: 'Oturumunuz zaman aşımına uğramış. Lütfen tekrar giriş yapın!' },
  403:{ message: 'Yetkisiz erişim! Lütfen sistem yöneticiniz ile irtibata geçin!' },
  formnotvalid:{ message:'Formdaki bilgiler eksik veya geçersiz. Lütfen form alanlarının gereksinimlerini kontrol edin!' },
  savenotsuccess:{ message:'Bir hata oluştu! Kayıt işlemi başarısız!' }
};
