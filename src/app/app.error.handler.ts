import { ErrorHandler, Injectable, Injector } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { ErrorService } from "./services/error.service";
import { NotificationService } from "./services/notification.service";
import { ErrorActions } from "./globals";
import { Router } from "@angular/router";

@Injectable()
export class AppErrorHandler implements ErrorHandler{
  constructor(private injector: Injector) {}

  handleError(error:any): void {
    const errorService = this.injector.get(ErrorService);
    const notifier = this.injector.get(NotificationService);
    const router = this.injector.get(Router);

    let message;
    let httpmessage;
    let stackTrace;

    if (error instanceof HttpErrorResponse) {
      // Server Error
      message = errorService.getServerMessage(error);
      stackTrace = errorService.getServerStack(error);
      if(error.status === 401) {
        if(error.error && error.error.errors.message){
          notifier.showError(error.error.errors.message);
        }else{
          notifier.showError(ErrorActions["401"]);
        }
        router.navigate(['/'],{
          queryParams:{
            returnUrl: router.url
          }
        });
      }else if(error.status==403){
        if(error.error && error.error.errors.message) {
          notifier.showError(error.error.errors.message);
        }else {
          notifier.showError(ErrorActions['403']);
        }
      }else {
        httpmessage = error.error.errors?error.error.errors.message:error.message;
        notifier.showError(httpmessage);
      }
    } else {
      // Client Error
      message = errorService.getClientMessage(error);
      stackTrace = errorService.getClientStack(error);
    }

    // Always log errors
    console.log(message, stackTrace);
    console.log(httpmessage, stackTrace);
    //console.error(error);
  }
}
