import { BrowserModule } from '@angular/platform-browser';
import {ApplicationRef, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule} from '@angular/core';

import { approuting } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutModule } from "./shared/layout/layout.module";
import {APP_RESOLVER_PROVIDERS} from "./app.resolver";
import {AppState} from "./app.service";
import {ErrorService} from "./services/error.service";
import {AuthModule} from "./auth/auth.module";
import {ServerErrorInterceptor} from "./services/server-error.inteceptors";
import {AppErrorHandler} from "./app.error.handler";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {NotificationService} from "./services/notification.service";

const APP_PROVIDERS = [...APP_RESOLVER_PROVIDERS, AppState];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AuthModule,
    BrowserModule,
    LayoutModule,
    approuting
  ],
  providers: [
    APP_PROVIDERS,
    ErrorService,
    NotificationService,
    { provide:ErrorHandler, useClass:AppErrorHandler },
    { provide:HTTP_INTERCEPTORS, useClass:ServerErrorInterceptor, multi:true }
  ],
  exports: [
    AppComponent
  ],
  bootstrap: [AppComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(public appRef:ApplicationRef, public appState:AppState) {
  }
}
