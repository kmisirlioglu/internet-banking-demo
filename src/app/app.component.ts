import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";
import * as globals from "./globals";
import {ComponentCommunication} from "./services/component-communication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy{
  constructor(public service:ComponentCommunication, public titleService:Title, private route:ActivatedRoute) {

    this.service.checkAuthenticated();
    let title = globals.siteTitle+(route.snapshot.data.pageTitle?' | '+route.snapshot.data.pageTitle:'');
    titleService.setTitle(title);
  }
  ngOnDestroy() {

  }
}
