import {Injectable, Injector} from "@angular/core";
import {HttpErrorResponse} from "@angular/common/http";
import {NotificationService} from "./notification.service";


@Injectable({ providedIn:'root' })
export class ErrorService {
  constructor(private injector: Injector) {}
  throw(error: any):any{
    const notifier = this.injector.get(NotificationService);
    let err;
    return new Promise((resolve, reject )=>{
      if(typeof error === 'object'){
        err = error;
      }else{
        err = { message:error }
      }
      if(err.showNotification == undefined || (err.showNotification==true)){
        notifier.showError(err);
      }
      throw new Error(err.message);
    });
  }

  getClientMessage(error: Error): string {
    if (!navigator.onLine) {
      return 'No Internet Connection';
    }
    return error.message ? error.message : error.toString();
  }

  getClientStack(error: Error): any {
    return error.stack;
  }

  getServerMessage(error: HttpErrorResponse): string {
    return error.message;
  }

  getServerStack(error: HttpErrorResponse): string {
    // handle stack trace
    return 'stack';
  }
}


