import { Injectable} from '@angular/core';
import { NotificationService as NotifyPrompt } from "../shared/utils/notification.service";
import {Router} from "@angular/router";

@Injectable({ providedIn:'root' })
export class NotificationService {

  constructor(private router: Router, public notifyPrompt:NotifyPrompt) { }

  showSuccess(data: any): void {
    let dobj:any = { message: null, redirect:null, id:null } ;
    if(typeof data === "string"){
      dobj.message = data;
    }
    if(typeof data === "object"){
      dobj = data;
    }
    if(dobj.redirect){
      let redirect = (typeof dobj.redirect == "string"?[dobj.redirect]:dobj.redirect);
      this.router.navigate(redirect);
    }
    this.notifyPrompt.notify({
      message: dobj.message + (dobj.id?' ID:'+dobj.id:''),
      type: 'success'
    }, () => {
      if(dobj.redirect){
        let redirect = (typeof dobj.redirect == "string"?[dobj.redirect]:dobj.redirect);
        this.router.navigate(redirect);
      }
    });
  }

  showWarn(data: any): void {
    let dobj:any = { message: null, redirect:null, id:null } ;
    if(typeof data === "string"){
      dobj.message = data;
    }
    if(typeof data === "object"){
      dobj = data;
    }
    this.notifyPrompt.notify({
        message: dobj.message + (dobj.id?' ID:'+dobj.id:''),
        type: 'warning'
      }, () => {
        if(dobj.redirect){
          let redirect = (typeof dobj.redirect == "string"?[dobj.redirect]:dobj.redirect);
          this.router.navigate(redirect);
        }
      }
    );
  }

  showError(data: any): void {
    let dobj:any = { message: null, redirect:null, id:null } ;
    if(typeof data === "string"){
      dobj.message = data;
    }
    if(typeof data === "object"){
      dobj = data;
    }
    if(dobj.redirect){
      let redirect = (typeof dobj.redirect == "string"?[dobj.redirect]:dobj.redirect);
      this.router.navigate(redirect);
    }
    // The second parameter is the text in the button.
    // In the third, we send in the css class for the snack bar.
    this.notifyPrompt.notify({
      message: "" + dobj.message + (dobj.id?' ID:'+dobj.id:''),
      type:'error'
    },() => {
      if(dobj.redirect){
        let redirect = (typeof dobj.redirect == "string"?[dobj.redirect]:dobj.redirect);
        this.router.navigate(redirect);
      }
    });
  }

  confirmBox(data?:any){
    this.notifyPrompt.alertBox({
      icon : data.icon,
      title : data.title,
      html : data.message,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'İptal',
      confirmButtonText: 'Evet'
    }, (result:any) => {
      if (result.value) {
        if(data.trueCallback && typeof data.trueCallback == 'function') data.trueCallback();
      }else{
        if(data.falseCallback && typeof data.falseCallback == 'function') data.falseCallback();
      }
    });
  }

  alertBox(data?:any){
    this.notifyPrompt.alertBox({
      icon : data.icon,
      title : data.title,
      html : data.message
    });
  }
}
