import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { ComponentCommunication } from "./component-communication.service";
import {ErrorService} from "./error.service";

@Injectable({ providedIn:'root' })
export class AuthGuardService implements CanActivate, CanActivateChild, Resolve<any> {

  constructor(private router: Router, private com:ComponentCommunication, private errorService:ErrorService) {}

  canActivate(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if (localStorage.getItem('currentusertoken')!==null) {
      return true;
    } else {
      this.router.navigate(['/'], {
        queryParams: {
          returnUrl: state.url
        }
      });
      return false;
    }
  }
  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    let permissions = null;
    /* This state */
    const childstate = childRoute.children[0];
    if(childstate && !childstate.children.length) permissions = childstate.data.permission;
    /**/
    if (localStorage.getItem('currentusertoken')!==null) {
      /*if(permissions){
        if(this.com.hasProperty(permissions)) {
          return true;
        }else{
          this.errorService.throw('Yasak: Yetkisiz erişim!');
          return false;
        }
      }*/
      return true;
    } else {
      this.router.navigate(['/'], {
        queryParams: {
          returnUrl: state.url
        }
      });
      return false;
    }
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
  }
}
