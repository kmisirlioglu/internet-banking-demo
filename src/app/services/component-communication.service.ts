/* tslint:disable */
import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

@Injectable({ providedIn:'root' })
export class ComponentCommunication {

  public authenticatedVal:boolean = false;
  public authenticated: Subject<boolean> = new Subject<boolean>();
  public title: Subject<any> = new Subject<any>();
  public loader: Subject<any> = new Subject<any>();
  permissions = {};

  constructor() {
    this.authenticated.subscribe((value) => {
      this.authenticatedVal = value;
    });
  }

  checkAuthenticated(){
    if(localStorage.getItem('currentusertoken')) {
      this.authenticatedVal = true;
      this.authenticated.next(this.authenticatedVal);
    }else{
      this.authenticatedVal = false;
      this.authenticated.next(this.authenticatedVal);
    }
  }
}
