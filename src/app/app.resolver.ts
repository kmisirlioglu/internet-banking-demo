import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import {NgModule} from "@angular/core";

@Injectable()
export class DataResolver implements Resolve<any> {
  constructor() {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return null;
  }
}
export const APP_RESOLVER_PROVIDERS = [
  DataResolver
];
